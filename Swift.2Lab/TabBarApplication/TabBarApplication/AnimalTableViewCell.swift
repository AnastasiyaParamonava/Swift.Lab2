//
//  AnimalTableViewCell.swift
//  TabBarApplication
//
//  Created by Admin on 17.02.17.
//  Copyright © 2017 paranastasia. All rights reserved.
//

import UIKit

class AnimalTableViewCell: UITableViewCell {

    @IBOutlet weak var animalImage: UIImageView!
    
    @IBOutlet weak var animalShortDescription: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

}
